package szerjavic.test.apache.poi;

import szerjavic.test.apache.poi.exception.FileNotFoundException;
import szerjavic.test.apache.poi.exception.ValueNotInDocumentException;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Set;

abstract class DocumentService {

    static final String TARGET_FOLDER = "target/";

    abstract void checkIfFileContainsValues(String fileName, Set<String> values) throws Exception;

    abstract void setPlaceholderValue(String fileName, Map<String, String> placeholderValues) throws Exception;

    File getFile(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL url = classLoader.getResource(fileName);
        if (url == null) {
            throw new FileNotFoundException();
        }

        File file = new File(url.getFile());
        if (!file.exists()) {
            throw new FileNotFoundException();
        }

        return file;
    }

    static void checkIfTextContainsAllValues(String text, Set<String> textToSearch) {

        textToSearch.forEach(valueToSearch -> {
            if (!text.contains(valueToSearch)) {
                throw new ValueNotInDocumentException();
            }
        });

    }

}

package szerjavic.test.apache.poi;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

class HWPFService extends DocumentService {

    @Override
    void checkIfFileContainsValues(String fileName, Set<String> values) throws Exception {

        File file = getFile(fileName);

        try (InputStream is = new FileInputStream(file)) {

            HWPFDocument document = new HWPFDocument(is);
            Range range = document.getRange();

            String text = range.text();

            checkIfTextContainsAllValues(text, values);
        }

    }

    @Override
    void setPlaceholderValue(String fileName, Map<String, String> placeholderValues) throws Exception {

        File file = getFile(fileName);

        try (InputStream is = new FileInputStream(file)) {

            HWPFDocument document = new HWPFDocument(is);
            Range range = document.getRange();

            String text = range.text();

            checkIfTextContainsAllValues(text, placeholderValues.keySet());

            placeholderValues.forEach((key, value) -> range.replaceText(key, value));
            document.write(new File(TARGET_FOLDER + fileName));
        }
    }


}

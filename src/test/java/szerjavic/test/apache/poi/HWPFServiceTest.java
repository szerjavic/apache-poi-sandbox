package szerjavic.test.apache.poi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import szerjavic.test.apache.poi.exception.FileNotFoundException;
import szerjavic.test.apache.poi.exception.ValueNotInDocumentException;

import java.util.HashMap;
import java.util.Map;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HWPFServiceTest {

    private static final String PLACE_HOLDER_VALUE = "data from place holder";
    private static final String PLACEHOLDER_KEY = "Placeholder1";
    private String DOCUMENT_WITH_PLACEHOLDER = "document.doc";

    private DocumentService hwpfService;

    @BeforeAll
    public void init() {
        hwpfService = new HWPFService();
    }


    @Test
    public void placeholderSuccess() throws Exception {
        Map<String, String> placeholderValues = new HashMap<>();
        placeholderValues.put(PLACEHOLDER_KEY, PLACE_HOLDER_VALUE);

        hwpfService.setPlaceholderValue(DOCUMENT_WITH_PLACEHOLDER, placeholderValues);

    }

    @Test
    public void placeholderFail() throws Exception {
        Map<String, String> placeholderValues = new HashMap<>();
        placeholderValues.put("NO_PLACEHOLDER", PLACE_HOLDER_VALUE);

        Assertions.assertThrows(ValueNotInDocumentException.class, () -> {
            hwpfService.setPlaceholderValue(DOCUMENT_WITH_PLACEHOLDER, placeholderValues);
        });
    }

    @Test
    public void fileNotFoundException() throws Exception {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            hwpfService.setPlaceholderValue("nodocument", null);
        });
    }


}